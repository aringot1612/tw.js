const { SlashCommandBuilder } = require('discord.js');

class Command {
  constructor(name, desc) {
    this.name = name;
    this.desc = desc;
    this.build();
  }

  build() {
    this.instance = new SlashCommandBuilder()
      .setName(this.name)
      .setDescription(this.desc)
  }

  get() {
    return this.instance;
  }
}

module.exports = { Command };