const { ActionRowBuilder, StringSelectMenuBuilder } = require('discord.js');

class Selector {
  constructor(lang, langHandler) {
    this.langHandler = langHandler;
    this.id = 'select';
    this.default = lang;
    this.emptyText = this.langHandler.get(lang, 'selectorEmpty');
    this.selector = null;
    this.build();
  }

  build() {
    this.selector = new ActionRowBuilder()
      .addComponents(new StringSelectMenuBuilder()
        .setCustomId(this.id)
        .setPlaceholder(this.emptyText)
        .addOptions(
          {
            label: this.langHandler.get('fr', 'min'),
            description: this.langHandler.get('fr', 'full'),
            value: this.langHandler.get('fr', 'min'),
            default: (this.default === this.langHandler.get('fr', 'min'))
          },
          {
            label: this.langHandler.get('en', 'min'),
            description: this.langHandler.get('en', 'full'),
            value: this.langHandler.get('en', 'min'),
            default: (this.default === this.langHandler.get('en', 'min'))
          }
        )
      );
  }

  get() {
    return (this.selector);
  }

  static async submit(interaction, user, langHandler) {
    const selected = interaction.values[0];
    await user.switchLang(selected);
    await interaction.reply(langHandler.get(selected, 'langSave'));
  }
}

module.exports = { Selector };