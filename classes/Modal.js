const { ActionRowBuilder, TextInputStyle, ModalBuilder, TextInputBuilder } = require('discord.js');
const { TriggerWarningBO } = require('../types/Types')

class Modal {
  constructor(lang, langHandler) {
    this.id = 'modal';
    this.inputId = 'twInput';
    this.langHandler = langHandler;
    this.title = this.langHandler.get(lang, 'modalTitle');
    this.desc = this.langHandler.get(lang, 'modalDesc');
    this.modal = null;
    this.input = null;
    this.action = null;
    this.build();
  }

  build() {
    this.modal = new ModalBuilder()
      .setCustomId(this.id)
      .setTitle(this.title);
    this.input = new TextInputBuilder()
      .setCustomId(this.inputId)
      .setLabel(this.desc)
      .setStyle(TextInputStyle.Paragraph);
    this.action = new ActionRowBuilder().addComponents(this.input);
    this.modal.addComponents(this.action);
  }

  get() {
    return (this.modal);
  }

  static async submit(interaction, user, langHandler) {
    const tw = interaction.fields.getTextInputValue('twInput');
    const twList = tw.split(/[,]+[ ]*/);
    if (twList.length > 0) {
      for (const tw of twList) {
        let twObject = await TriggerWarningBO.findOne({ where: { value: tw } });
        if (twObject === null) {
          twObject = await TriggerWarningBO.create({
            value: tw.toLowerCase().trim()
          });
        }
        await user.addTW(twObject);
      }
    }
    await interaction.reply(langHandler.get(user.getLang(), 'twSave'));
  }
}

module.exports = { Modal };