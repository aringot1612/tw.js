const { UserBO, TWLinkBO } = require('../types/Types')

class User {
  constructor() {
    UserBO.sync();
    this.userObj = null;
    this.lang = 'fr';
  }

  async retrieveUser(interaction) {
    var interactionUser = await interaction.guild.members.fetch(interaction.user.id);
    await this.createUser(interactionUser.id, interactionUser.user.username);
  }

  addTW(tw) {
    this.userObj.addTriggerWarning(tw, { through: TWLinkBO });
  }

  switchLang(newLang) {
    this.userObj.update({ lang: newLang });
  }

  getLang() {
    return this.userObj.get('lang');
  }

  async createUser(userId, userName) {
    await UserBO.findOne({ where: { userId: userId } }).then(res => {
      this.userObj = res;
    });
    if (this.userObj === null) {
      await UserBO.create({
        username: userName,
        userId: userId,
        lang: this.lang
      }).then(res => {
        this.userObj = res;
      });
    }
  }

  get() {
    return this.userObj;
  }
}

module.exports = { User };