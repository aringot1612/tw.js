const fs = require('node:fs');
const { REST, Routes } = require('discord.js');

class Commands {
  constructor() {
    this.commands = [];
    this.commandFiles = fs.readdirSync('./commands/').filter(file => file.endsWith('.js'));
    for (const file of this.commandFiles) {
      const command = require(`../commands/${file}`);
      this.commands.push(command.data.toJSON());
    }
    this.rest = new REST({ version: '10' }).setToken(process.env.DISCORD_TOKEN);
    (async () => {
      try {
        const data = await this.rest.put(
          Routes.applicationGuildCommands(process.env.CLIENT_ID, process.env.GUILD_ID),
          { body: this.commands },
        );
      } catch (error) {
        console.error(error);
      }
    })();
  }

  get() {
    return this.commands;
  }
}

module.exports = { Commands };