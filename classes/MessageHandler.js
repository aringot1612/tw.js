const { TriggerWarningBO, UserBO } = require('../types/Types')

class MessageHandler {
  constructor(client, langHandler) {
    this.msg = null;
    this.privateMessage = null;
    this.client = client;
    this.langHandler = langHandler;
    this.replaceMask = '...';
    this.idReg = /<@([0-9]*)>/g;
  }

  async handle(msg, client) {
    var arr = [];
    this.msg = msg;
    if (msg.author.bot) return;
    const records = await TriggerWarningBO.findAll();
    var promises = records.map(function (record) {
      return new Promise(function (resolve, reject) {
        if (msg.content.toLowerCase().includes(record.dataValues.value)) {
          msg.react('⚠️');
          const targets = TriggerWarningBO.findOne({
            where: { value: record.dataValues.value },
            include: UserBO
          });
          targets.then(target => {
            for (const user of target.dataValues.users) {
              var existing = arr.find((e) => e.user.dataValues.userId === user.dataValues.userId);
              if (existing !== undefined) {
                if (!existing.tw.includes(record.dataValues.value)) {
                  existing.tw.push(record.dataValues.value);
                }
                resolve();
              } else {
                arr.push({ "user": user, "tw": [record.dataValues.value] });
                resolve();
              }
            }
          })
        } else {
          resolve();
        }
      });
    });
    Promise.all(promises)
      .then(() => {
        for (const obj of arr) {
          this.sendPrivate(obj, client);
        }
      })
      .catch(console.error);
  }

  async sendPrivate(obj, client) {
    if (this.msg === null) return;
    if (obj.tw.length < 1) return;
    var newMessage = this.msg.content;
    var fullMessage = "";
    for (const trigger of obj.tw) {
      newMessage = await this.replaceCustom(newMessage, trigger);
    }
    var datetime = new Date(this.msg.createdAt);
    var date = datetime.toLocaleDateString("fr");
    var time = datetime.toLocaleTimeString("fr");
    var channel = this.client.channels.cache.find(channel => channel.id === this.msg.channelId).name;
    var mpType = (obj.tw.length === 1 ? 'privateMessage' : 'privateMessageOpt');
    fullMessage = this.langHandler.get(obj.user.dataValues.lang, mpType)
      .format(date, time, this.msg.author.username, channel, newMessage);
    client.users.fetch(obj.user.dataValues.userId, false).then((usr) => {
      usr.send(fullMessage);
    });
  }

  async replaceCustom(str, trigger) {
    var regEx = new RegExp(trigger, "ig");
    return this.replaceId(str.replace(regEx, this.replaceMask));
  }

  async replaceId(str){
    var array = str.matchAll(this.idReg);
    if(array.length < 1) return str;
    try {
      for(const match of array){
        let userOpt = await this.client.users.fetch(match[1]);
        str = str.replace(match[0], userOpt.username);
      }
    } catch (error){
      return str;
    }
    return str;
  }
}

module.exports = { MessageHandler };