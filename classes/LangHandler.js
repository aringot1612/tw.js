const fr = require('../lang/fr_FR.json');
const en = require('../lang/en_EN.json');

String.prototype.format = function () {
  var formatted = this;
  for (var i = 0; i < arguments.length; i++) {
    var regexp = new RegExp('\\{' + i + '\\}', 'gi');
    formatted = formatted.replace(regexp, arguments[i]);
  }
  return formatted;
};

class LangHandler {
  constructor() { }

  get(lang, value) {
    switch (lang) {
      case 'en':
        return en[value];
        break;
      case 'fr':
        return fr[value];
        break;
    }
  }
}

module.exports = { LangHandler };