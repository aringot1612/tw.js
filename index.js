// Requirements.
const { Client, GatewayIntentBits, Events } = require('discord.js');
const { TriggerWarningBO, UserBO, TWLinkBO } = require('./types/Types')
const { User } = require('./classes/User');
const { Modal } = require('./classes/Modal');
const { Selector } = require('./classes/Selector');
const { MessageHandler } = require('./classes/MessageHandler');
const { LangHandler } = require('./classes/LangHandler');
const { Commands } = require('./classes/Commands');
const dotenv = require('dotenv');

// Env variable config.
dotenv.config();

// Client instance.
const client = new Client({
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.GuildMessageReactions,
    GatewayIntentBits.GuildMessageTyping,
    GatewayIntentBits.DirectMessages,
    GatewayIntentBits.DirectMessageReactions,
    GatewayIntentBits.DirectMessageTyping,
    GatewayIntentBits.MessageContent
  ]
})

// Sequelize Sync.
client.on('ready', () => {
  UserBO.sync();
  TriggerWarningBO.sync();
  TWLinkBO.sync();
});

// Classes.
var user = new User();
const langHandler = new LangHandler();
const messageHandler = new MessageHandler(client, langHandler);
const commands = new Commands();

// On command trigger.
client.on(Events.InteractionCreate, async interaction => {
  if (!interaction.isChatInputCommand()) return;
  // Get current user.
  await user.retrieveUser(interaction);
  if (interaction.commandName === 'tw') {
    // Modal build
    var modal = new Modal(user.getLang(), langHandler);
    await interaction.showModal(modal.get());
  } else if (interaction.commandName === 'lang') {
    // Selector build
    var selector = new Selector(user.getLang(), langHandler);
    await interaction.reply({ components: [selector.get()], ephemeral: true });
  }
});

// On Modal submit.
client.on(Events.InteractionCreate, async interaction => {
  if (!interaction.isModalSubmit()) return;
  // Get current user.
  user.retrieveUser(interaction);
  // Save TW.
  Modal.submit(interaction, user, langHandler);
});

// On Lang selector submit.
client.on(Events.InteractionCreate, async interaction => {
  if (!interaction.isStringSelectMenu()) return;
  // Get current user.
  user.retrieveUser(interaction);
  // Save lang.
  Selector.submit(interaction, user, langHandler);
});

client.on('messageCreate', async (msg) => {
  try {
    // Check messages and handle potentials tw.
    messageHandler.handle(msg, client);
  } catch (error) {
    console.error(error);
  }
});

// Client instance logged to server
client.login(process.env.DISCORD_TOKEN);