const { Command } = require('../classes/Command');

class TW extends Command {
  constructor() {
    super('tw', 'Configuration du trigger warning')
  }
}

const tw = new TW();
module.exports = {
  data: tw.get()
};