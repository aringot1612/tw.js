const { Command } = require('../classes/Command');

class Lang extends Command {
  constructor() {
    super('lang', 'Configuration de la langue')
  }
}

const lang = new Lang();
module.exports = {
  data: lang.get()
};