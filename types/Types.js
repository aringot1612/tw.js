const { sequelize, Sequelize } = require('../services/connector')

const TriggerWarningBO = sequelize.define('triggerWarning', {
  value: {
    type: Sequelize.STRING,
    unique: true
  }
});

const UserBO = sequelize.define('user', {
  lang: Sequelize.STRING,
  username: Sequelize.STRING,
  userId: {
    type: Sequelize.STRING,
    unique: true
  }
});

const TWLinkBO = sequelize.define('TWLink', {});

UserBO.belongsToMany(TriggerWarningBO, { through: TWLinkBO });
TriggerWarningBO.belongsToMany(UserBO, { through: TWLinkBO });

module.exports = { UserBO, TriggerWarningBO, TWLinkBO }